package main

import (
	"github.com/marcelosousa/dev-fest-demo/cmd"
)

func main() {
	cmd.Execute()
}
